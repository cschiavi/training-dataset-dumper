#!/usr/bin/env python

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

import json
import sys

from AthenaConfiguration.ComponentFactory import CompFactory
from GaudiKernel.Constants import INFO

from FTagDumper import dumper
from FTagDumper import mctc
from FTagDumper import trigger as trig


def get_args():
    pflow_chain = 'HLT_j20_0eta290_020jvt_boffperf_pf_ftf_L1J15'
    emtopo_chain = 'HLT_j20_roiftf_preselj20_L1RD0_FILLED'
    tau_chain='HLT_tau20_perf_tracktwoMVA_L1eTAU12'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('--pflow-chain', default=pflow_chain, **dh)
    parser.add_argument('--emtopo-chain', default=emtopo_chain, **dh)
    parser.add_argument('--tau-chain', default=tau_chain, **dh)

    return parser.parse_args()

def run():
    args = get_args()


    flags = dumper.update_flags(args)
    flags.lock()

    # parse the configurations
    combined_cfg = dumper.combinedConfig(args.config_file)
    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = dumper.getMainConfig(flags)
    
    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)
    chains = {
        'pflow' : args.pflow_chain,
        'emtopo' : args.emtopo_chain,
        'tau' : args.tau_chain,
    }
    trig_flag_funcs = {
        'pflow' : trig.pflowDumper,
        'emtopo' : trig.emtopoDumper,
        'tau' : trig.tauDumper,
    }
    
    
    
    is_data=None
    for constituent in ["pflow", "emtopo", "tau"]:

        if constituent not in combined_cfg:
            print("Trigger constituent ", constituent, "not found in config - skipping")
            continue
        # If using a 'combined' config e.g. trigger_all_sampleA the combined config
        # will be like {pflow : {pflow : config}} to work with standard config merging
        if constituent in combined_cfg[constituent]:
            constituent_cfg = combined_cfg[constituent][constituent]
        else:
            constituent_cfg = combined_cfg[constituent]
        
        if is_data is None:
            is_data = constituent_cfg['data']
            if not is_data:
                ca.merge(mctc.getMCTC())
        elif is_data != constituent_cfg['data']:
            raise ValueError("All constituent configs must have the same value for 'data'")
        cfg, dumper_cfg = trig.getJobConfig(constituent_cfg)

        # force both chains to pass
        cfg["additional_chains"] = cfg["additional_chains"] + [chains[constituent]]
        
        # if constituent
        ca.merge(dumper.parseBlocksConfig(dumper_cfg, flags))
        ca.merge(trig_flag_funcs[constituent](
            flags,
            chain=chains[constituent],
            **cfg
        ))

        ca.addEventAlgo(CompFactory.JetDumperAlg(
            constituent + 'Dumper',
            output=output,
            configJson=json.dumps(dumper_cfg),
            forceFullPrecision=args.force_full_precision,
            group=constituent))
    
    

    # Set Alg sequence to seqAND, no other algorithms shall be merged after
    dumper.setMainAlgSeqToSeqAND(ca)
    # print the configuration
    if flags.Exec.OutputLevel <= INFO:
        ca.printConfig(withDetails=True)

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)

from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class TrackFlowOverlapRemoval(BaseBlock):
    """
    Matches a jet collection to a b-tagging collection.
    
    Parameters
    ----------
    jet_collection : str
        The name of the jet collection to match. If not provided, it will be taken from the dumper config.
    """

    jet_collection: str = None
    track_container: str = None
    name: str = "nonConstituentAdder"

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config['jet_collection']



    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            CompFactory.TrackFlowOverlapRemovalAlg(
                name=self.name,
                Tracks=f'{self.jet_collection}.{self.track_container}',
                Constituents=f'{self.jet_collection}.constituentLinks',
                OutTracks=f'{self.jet_collection}.nonConstituentTracks'
            )
        )
        
        return ca
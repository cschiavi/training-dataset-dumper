atlas_subdir(EventSelectionCounting)

set(LINK_LIBRARIES
  SelectionHelpersLib
  AthenaBaseComps
)

atlas_add_library(event_selection_counting
  src/EventCounterAlg.cxx
  src/EventSelectionCounterAlg.cxx
  PUBLIC_HEADERS EventSelectionCounting
  LINK_LIBRARIES ${LINK_LIBRARIES}
)

atlas_add_component(EventSelectionCounting
  src/EventCounterAlg.cxx
  src/EventSelectionCounterAlg.cxx
  src/components.cxx
  LINK_LIBRARIES event_selection_counting ${LINK_LIBRARIES}
)

atlas_install_python_modules(
  python/*.py
  POST_BUILD_CMD ${ATLAS_FLAKE8} --extend-ignore=ATL902
)

#include "EventSelectionCounting/EventSelectionCounterAlg.h"

#include <nlohmann/json.hpp>

#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

EventSelectionCounterAlg::EventSelectionCounterAlg(const std::string &name, ISvcLocator* loc):
  EL::AnaAlgorithm(name, loc), m_counts()
{}

std::string EventSelectionCounterAlg::selection_label(std::string name) {
  std::string label = name;
  if (name.ends_with("_%SYS%")) {
    label = name.substr(0, name.length() - strlen("_%SYS%"));
  }
  return name;
}

StatusCode EventSelectionCounterAlg::initialize() {
  ANA_CHECK(m_event_info_handle.initialize(m_systematics_list));
  ANA_CHECK(m_selections.initialize(m_systematics_list, m_event_info_handle));
  ANA_CHECK(m_systematics_list.initialize());
  ANA_CHECK(m_selection_name_svc.retrieve());

  if (m_counts_output_path.value().empty()) {
    ATH_MSG_ERROR("Event counter file name not given");
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode EventSelectionCounterAlg::execute() {
  auto systematic_handles = m_systematics_list.systematicsVector();
  if (systematic_handles.size() != 1) {
    ANA_MSG_ERROR(
      "expected systematic handles vector to have size 1,"
      << " to not try to support something half-heartedly before it is needed;"
      << " found size " << systematic_handles.size() << " instead"
    );
  }
  auto systematic_handle = systematic_handles.at(0);

  const xAOD::EventInfo *event_info = nullptr;
  ANA_CHECK(m_event_info_handle.retrieve(event_info, systematic_handle));

  if (m_include_total_count.value())
    ++m_counts["total"];

  for (size_t i = 0; i < m_selections.size(); ++i) {
    std::string label;
    ANA_CHECK(m_systematics_list.service().makeSystematicsName(
      label, m_selections.at(i).getSelectionName(), systematic_handle
    ));

    if (m_selections.at(i).getBool(*event_info, systematic_handle)) {
      ++m_counts[label];
    } else if (m_counts.find(label) == m_counts.end()) {
      m_counts[label];
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode EventSelectionCounterAlg::finalize() {
  using CountsMap = std::vector<std::pair<std::string, unsigned long long>>;

  nlohmann::json counts_json;
  if (fs::exists(m_counts_output_path.value())) {
    std::ifstream counts_stream(m_counts_output_path.value());
    counts_json = nlohmann::json::parse(counts_stream);
  } else {
    counts_json = CountsMap();
  }

  for (const auto& [name, count] : counts_json.get<CountsMap>()) {
    if (m_counts.find(name) != m_counts.end()) {
      ANA_MSG_ERROR("count name " << name
          << " already exists in " << m_counts_output_path.value());
      return StatusCode::FAILURE;
    }
  }

  CountsMap counts_list;
  ANA_CHECK(convert_counts_to_list(std::move(m_counts), counts_list));
  for (const auto& [name, count] : counts_list) {
    counts_json.push_back({name, count});
  }

  std::ofstream counts_stream(m_counts_output_path.value());
  if (!counts_stream) {
    ANA_MSG_ERROR("Failed to open output file: " << m_counts_output_path.value());
    return StatusCode::FAILURE;
  }

  counts_stream << counts_json.dump(4);
  return StatusCode::SUCCESS;
}

StatusCode EventSelectionCounterAlg::convert_counts_to_list(
  std::map<std::string, unsigned long long> counts,
  std::vector<std::pair<std::string, unsigned long long>>& counts_list
) {
  counts_list.resize(counts.size());

  if (m_include_total_count.value())  {
    auto total_pos = counts.find("total");
    if (total_pos == counts.end()) {
      ANA_MSG_ERROR("expected total to exists in counts");
      return StatusCode::FAILURE;
    }

    counts_list[0] = *total_pos;
    counts.erase(total_pos);
  }

  for (const auto& [name, count] : counts) {
    std::istringstream name_stream(name);

    std::string selection_name, subselection_name, selection_index;
    std::getline(name_stream, selection_name, '_');
    std::getline(name_stream, subselection_name, '_');
    std::getline(name_stream, selection_index, '_');

    size_t index = -1;
    try {
      index = m_include_total_count ?
        std::stoi(selection_index) : std::stoi(selection_index) - 1;
    } catch (std::exception& e) {
      ANA_MSG_ERROR("expected token " << selection_index << " to be a number: " << e.what());
      return StatusCode::FAILURE;
    }

    if (index >= counts_list.size()) {
      ANA_MSG_ERROR("expected index " << index << " to be in bounds of counts_list");
      return StatusCode::FAILURE;
    }

    std::ostringstream name_builder;
    name_builder << selection_name << "_" << subselection_name << "_" << selection_index;

    counts_list[index] = { name_builder.str(), count };
  }

  return StatusCode::SUCCESS;
}
